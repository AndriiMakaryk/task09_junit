package com.makaryk.tasks.tasks;

public interface SimpleCalculator {
    public double add(double a, double b);

    public int minus(int a, int b);

    public int mult(int a, int b);

    public int div(int a, int b);
}
