package com.makaryk.tasks.tasks;

public class Calculator  {
     SimpleCalculator calculator;

    public Calculator() {
    }

    public Calculator(SimpleCalculator calculator) {
        this.calculator = calculator;
    }

    public int adding(double a,double b) {
        double result=calculator.add( a, b);
        return result;
    }

    public int multypling(int a,int b){
        return calculator.mult(a,b);
    }
    public int dividing(int a,int b){
        return calculator.div(a,b);
    }

}
